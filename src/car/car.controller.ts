import { Controller, Post, Body, Get } from '@nestjs/common';
import { CommandBus, QueryBus } from '@nestjs/cqrs';
import { CreateCarCommand } from './commands/createCarCommand';
import { ApiTags } from '@nestjs/swagger';
import { CreateCarDto } from './dto/createCar.dto';
import { CarDto } from './dto/carDto';
import { Car } from './model/car.model';
import { GetCarQuery } from './queries/getCarQuery';

@ApiTags('cars')
@Controller('cars')
export class CarController {
    constructor(
        private commanBus: CommandBus,
        private readonly queryBus: QueryBus,
        ) {}


    @Post()
    async createCar(@Body() createcar: CreateCarDto): Promise<Car> {
        return this.commanBus.execute(new CreateCarCommand(createcar));
    }

    @Get()
    async getAsync(): Promise<Car> {
        return this.queryBus.execute(new GetCarQuery());
    }
}
