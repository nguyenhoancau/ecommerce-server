import { IsUUID } from "class-validator";
import { ApiProperty } from "@nestjs/swagger";

export class CreateCarDto {
    @IsUUID()
    id: string;
    @ApiProperty()
    name: string;
    @ApiProperty()
    type: string;
    @ApiProperty()
    color: string;
}