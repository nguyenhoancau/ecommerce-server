export class CarDto {
    id: string;
    name: string;
    type: string;
    color: string;
}