import { QueryHandler, IQueryHandler } from "@nestjs/cqrs";
import { GetCarQuery } from "./getCarQuery";
import { CarRepository } from "../repo/carRepository";

@QueryHandler(GetCarQuery)
export class GetCarsHandler implements IQueryHandler<GetCarQuery> {
  constructor(private readonly repository: CarRepository) {}

  async execute(query: GetCarQuery) {
    console.log('Async GetHeroesQuery...');
    return this.repository.getAllAsync();
  }
}