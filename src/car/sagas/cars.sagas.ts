import { Injectable } from "@nestjs/common";
import { Saga, ICommand, ofType } from "@nestjs/cqrs";
import { Observable } from "rxjs";
import { delay, map } from 'rxjs/operators';
import { CreateCarEvent } from "../events/createCarEvent";
import { CreateCarCommand } from "../commands/createCarCommand";

@Injectable()
export class CarsSagas {
//   @Saga()
//   dragonKilled = (events$: Observable<any>): Observable<ICommand> => {
//     return events$
//       .pipe(
//         ofType(CreateCarEvent),
//         delay(1000),
//         map(event => {
//           console.log('Inside [HeroesGameSagas] Saga');
//           return new CreateCarCommand();
//         }),
//       );
//   }
}