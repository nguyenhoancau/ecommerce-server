import { AggregateRoot } from "@nestjs/cqrs";
import { CreateCarEvent } from "../events/createCarEvent";
import { CarDto } from "../dto/carDto";
import { CreateCarDto } from "../dto/createCar.dto";

export class Car extends AggregateRoot {

    constructor() {
      super();
      this.id = '';
      this.name = '';
      this.type = '';
      this.color = '';
    }
    
    id: string;
    name: string;
    type: string;
    color: string;
  
    createCar(car: CreateCarDto) {
      // logic
      this.apply(new CreateCarEvent(car));
    }

    updateCar(car: CreateCarDto) {

    }
  }