import { CarService } from './car.service';
import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { CarController } from './car.controller';
import { CarRepository } from './repo/carRepository';
import { CommandHandlers } from './commands';
import { EventHandlers } from './events';
import { QueryHandlers } from './queries';
import { MongooseModule } from '@nestjs/mongoose';
import { CarSchema } from './schema/car.schema';

@Module({
  imports: [
    CqrsModule,
    MongooseModule.forFeature([{ name: 'Car', schema: CarSchema }])
  ],
  controllers: [CarController],
  providers: [
    CarService,
    CarRepository,
    ...CommandHandlers,
    ...EventHandlers,
    ...QueryHandlers
  ],
  exports: [
    CqrsModule
  ]
})
export class CarModule {}
