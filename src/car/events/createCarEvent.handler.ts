import { EventsHandler, IEventHandler } from "@nestjs/cqrs";
import { CreateCarEvent } from "./createCarEvent";
import { CarRepository } from "../repo/carRepository";

@EventsHandler(CreateCarEvent)
export class CreateCarEventHandler implements IEventHandler<CreateCarEvent> {
    handle(event: CreateCarEvent) {
        console.log('CreateCarEvent');
    }
}