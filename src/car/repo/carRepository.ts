import { CarDto } from "../dto/carDto";
import { Model } from 'mongoose';
import { InjectModel } from "@nestjs/mongoose";
import { CreateCarDto } from "../dto/createCar.dto";
import { Car } from "../model/car.model";
import { Injectable } from "@nestjs/common";

@Injectable()
export class CarRepository {
    constructor(
        @InjectModel('Car') private readonly carModel: Model<Car>,
        ) {}

    async createCarAsync(car: CreateCarDto): Promise<Car> {
        const newProduct = await new this.carModel(car);
        const product = newProduct.save();
        return product;
    }

    async getAllAsync(): Promise<Car[]> {
        return await this.carModel.find().exec();
    }
}