import { CreateCarDto } from "../dto/createCar.dto";

export class CreateCarCommand {
    constructor(
        public car: CreateCarDto,
    ) {}
}