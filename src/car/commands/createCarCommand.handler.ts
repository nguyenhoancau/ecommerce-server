import { CreateCarCommand } from "./createCarCommand";
import { CommandHandler, ICommandHandler, EventPublisher } from "@nestjs/cqrs";
import { CarRepository } from "../repo/carRepository";

@CommandHandler(CreateCarCommand)
export class CreateCarCommandHandler implements ICommandHandler<CreateCarCommand> {
  constructor(
      private repository: CarRepository,
      private publisher: EventPublisher,
      ) {}

  async execute(command: CreateCarCommand) {
    const { car } = command;
    const newCar = this.repository.createCarAsync(car);

    (await newCar).createCar(car);
    (await newCar).commit();
  }
}