import { EventTypeEnum } from "./event-type.enum";

export class EventModel<T = any> {
    CommandId: string;
    Type: EventTypeEnum;
    MessageId: number = 0;
    ResponeEvent: EventTypeEnum;
    Payload: T;
    static Create<T>(type: EventTypeEnum, payload: T): EventModel<T> {
      return {
        Type: type,
        Payload: payload,
      } as EventModel<T>;
    }
    static From(msg: any): EventModel<any> {
      return msg as EventModel<any>;
    }
  }