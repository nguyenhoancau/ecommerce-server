export enum EventTypeEnum {
    ProductUpdatedState = 'product.updated.state',
    ProductCheckingState = 'product.checking.state',
    ProductResponeState = 'product.respone.state',
}