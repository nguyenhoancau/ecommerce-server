import dotenv from 'dotenv/config';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors();

  const options = new DocumentBuilder()
    .setTitle('Product Api')
    .setDescription('Product Api Description')
    .setVersion('0.1')
    .addTag('Products')
    .addServer('http://')
    .addServer('https://')
    .build();

  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('api',app, document);

  await app.listen(process.env.PORT || 3000);
}
bootstrap();
