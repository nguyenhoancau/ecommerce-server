import { Injectable } from "@nestjs/common";
import { ProductRepo } from "./repo/product.repo";
import {
    StateMachine, Machine, interpret, Interpreter, assign,
  } from 'xstate';
import { Product } from "./interface/product.interface";
import { ProductSchema } from "./schemas/Product.schema";
import { ProductDto } from "./dto/product.dto";
import { ProductStatusEnum } from "./product.enum";
import { EventModel } from "src/@shared/enums/event.interface";
import { EventTypeEnum } from "src/@shared/enums/event-type.enum";

@Injectable()
export class ProductService {
    private productStateMachine: StateMachine<any, any, any, any>;
    private service: Interpreter<any>;
    constructor(
        private readonly productRepo: ProductRepo,
    ) {
        let _this = this;
        this.productStateMachine = Machine<any>(
            {
              id: 'productState',
              initial: 'idle',
              context: {
                  product: null,
                  _status: null
              },
              states: {
                  idle: {
                      always: {
                          target: 'draft'
                      },
                  },
                  draft: {
                      on: {
                          ACTIVE: {target: 'active', cond: 'draftToActive', actions: 'toActive'},
                          REJECT: {target: 'reject', cond: 'draftToReject', actions: 'toReject'},
                        }
                  },
                  active: { 
                      on: {                   
                          INACTIVE: {
                              target: 'inactive',
                              cond: 'activeToInactive',
                              actions: 'toInactive'
                          },
                      }
                  },
                  inactive: {
                      type: 'final',
                  },
                  reject: {
                      type: 'final',
                  },
              }
            },
            {
              services: {
                // getNextState: (context, event) => async (
                //   callback,
                //   onReceive,
                // ) => {
                //   let _status = context['_status'];
                //   let productItem: ProductDto = context['product'] as ProductDto;
                //   let _event = EventModel.Create(EventTypeEnum.ProductUpdatedState, {
                //     current_state: productItem.status,
                //     next_state: _status,
                //   });
                // }
              },
              guards: {
                draftToActive: (context, event) => {
                  console.log(context);
                  let productItem: ProductDto = context['product'] as ProductDto;
                  let nextState: string = context['_status'];
                  return (
                    productItem.status === ProductStatusEnum.Draft &&
                    nextState === ProductStatusEnum.Active
                  );
                },
                draftToReject: (context, event) => {
                  console.log(context);
                  let productItem: ProductDto = context['product'] as ProductDto;
                  let nextState: string = context['_status'];
                  return (
                    productItem.status === ProductStatusEnum.Draft &&
                    nextState === ProductStatusEnum.Reject
                  );
                },
                activeToInactive: (context, event) => {
                  console.log(context);
                  let productItem: ProductDto = context['product'] as ProductDto;
                  let nextState: string = context['_status'];
                  return (
                    productItem.status === ProductStatusEnum.Active &&
                    nextState === ProductStatusEnum.Inactive
                  );
                },
              },
              actions: {
                toActive: assign(async (context, event) => {
                  console.log(context);
                  let productItem: ProductDto = context['product'] as ProductDto;
                  await _this.productRepo.updateStateAsync(
                    productItem.id,
                    ProductStatusEnum.Active,
                  );
                }),
                toInactive: assign(async (context, event) => {
                  console.log(context);
                  let productItem: ProductDto = context['product'] as ProductDto;
                  console.log(productItem.status);
                  console.log(productItem.id);
                  await _this.productRepo.updateStateAsync(
                    productItem.id,
                    ProductStatusEnum.Inactive,
                  );
                }),
                toReject: assign(async (context, event) => {
                  console.log(context);
                  let productItem: ProductDto = context['product'] as ProductDto;
                  await _this.productRepo.updateStateAsync(
                    productItem.id,
                    ProductStatusEnum.Reject,
                  );
                }),
              },
            },
        );
    };

    async getAllAsync(): Promise<Product[]> {
        return await this.productRepo.getAllAsync();
    }

    async getProductAsync(id: string): Promise<Product> {
        return await this.productRepo.getProductByIdAsync(id);
    }

    async createProductAsync(product: Product): Promise<Product> {
        return await this.productRepo.createProductAsync(product);
    }

    async updateProductAsync(id: string, updateProduct): Promise<Product> {
        return await this.productRepo.updateProductAsync(id, updateProduct)
    }

    async updateStatusAsync(
        id: string,
        _status: string,
        callBack: any,
        ): Promise<void> {
        let findProduct = await this.productRepo.getProductByIdAsync(id);
        const productStateMachine = this.productStateMachine.withContext({
            product: findProduct,
            _status: _status,
        });
        let counter = 0;
        
        this.service = interpret(productStateMachine).onTransition(
              state => {
                counter == 1 &&
                  callBack({
                    success: true,
                    message: "Your request's been recieved, please waiting",
                  });
                counter++;
              },
        );
        
        this.service.start(findProduct.status);
        let event = _status.toUpperCase();
        this.service.send(event);
    }

    async deleteProductAsync(id: string): Promise<any> {
        return await this.productRepo.deleteProductAsync(id);
    }
}