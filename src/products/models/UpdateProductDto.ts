import { ApiProperty } from "@nestjs/swagger";
import { IsUUID } from 'class-validator';

export class UpdateProductDto {
    @IsUUID()
    id: string;
    @ApiProperty()
    readonly title: string;
    @ApiProperty()
    readonly description: string;
    @ApiProperty()
    readonly price: number;

    constructor() {
        this.id = '';
        this.title = '';
        this.description = '';
        this.price = 0;
    }
}