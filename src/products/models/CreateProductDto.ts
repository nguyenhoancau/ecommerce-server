import { ApiProperty } from "@nestjs/swagger";

export class CreateProductDto {
    @ApiProperty()
    readonly title: string;
    readonly status: string;
    @ApiProperty()
    readonly description: string;
    @ApiProperty()
    readonly price: number;

    constructor() {
        this.title = '';
        this.status = 'draft';
        this.description = '';
        this.price = 0;
    }

}