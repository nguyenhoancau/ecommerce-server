export class ProductModel {
    id: string;
    title: string;
    status: string;
    description: string;
    price: number;
    constructor() {
      this.id = '';
      this.title = '';
      this.description = '';
      this.price = 0;
      this.status = 'draft';
    }
  }