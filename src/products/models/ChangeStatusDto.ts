import { ApiProperty } from "@nestjs/swagger";

export class ChangeStatusProduct {
    @ApiProperty()
    status: string;
}