import { plainToClass } from 'class-transformer';

export class ProductDto {
    id: string;
    title: string;
    status: string;
    description: string;
    price: number;

    constructor() {
      this.id = '';
      this.title = '';
      this.description = '';
      this.price = 0;
      this.status = 'draft';
    }

    static dummyData(): ProductDto[] {
        const products = [
          {
            id: '6bcd864e-424b-4d3d-bf96-3002444edc38',
            title: 'Iphone XS Max',
            description: 'dung lượng 512G',
            status: 'draft',
            price: 35000000,
          },
        ];
        return plainToClass(ProductDto, products);
    }
}