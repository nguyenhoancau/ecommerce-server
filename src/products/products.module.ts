import { Module } from '@nestjs/common';
import { ProductsController } from './product.controller';
import { ProductService } from './product.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ProductSchema } from './schemas/Product.schema';
import { ProductRepo } from './repo/product.repo';

@Module({
    imports: [
        MongooseModule.forFeature([{ name: 'Product', schema: ProductSchema}])
    ],
    controllers: [ProductsController],
    providers: [ProductService, ProductRepo]
})
export class ProductsModule {}
