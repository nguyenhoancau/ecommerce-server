import * as mongoose from 'mongoose';

export const ProductSchema = new mongoose.Schema ({
        title: String,
        status:  {
                type: String,
                default: 'draft'
        },
        description: String,
        price: Number
})  