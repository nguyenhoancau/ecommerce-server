export enum ProductStatusEnum {
    Draft = 'draft',
    Active = 'active',
    Inactive = 'inactive',
    Reject = 'reject',
  }
  