import { Controller, Post, Get, Body, Param, HttpException, HttpStatus, Put, Delete, Patch, Res } from '@nestjs/common';
import { ProductService } from './product.service';
import { CreateProductDto } from './models/CreateProductDto';
import { ApiTags, ApiParam } from '@nestjs/swagger';
import { Product } from './interface/product.interface';
import { UpdateProductDto } from './models/UpdateProductDto';
import { Response, Request } from 'express';
import { ChangeStatusProduct } from './models/ChangeStatusDto';

@ApiTags('products')
@Controller('products')
export class ProductsController {

    constructor(private productService: ProductService) {}

    @Get()
    async getAll(): Promise<Product[]> {
        return await this.productService.getAllAsync();
    }

    @ApiParam({ name: 'id'})
    @Get(':id')
    async getProduct(@Param('id') id): Promise<Product> {
        try {
        return await this.productService.getProductAsync(id);
        } catch (err) {
        throw new HttpException('Product Not Found', HttpStatus.NOT_FOUND);
        }
    }

    @Post()
    async postProduct(@Body() createProductDto: CreateProductDto): Promise<Product> {
        try {
        return await this.productService.createProductAsync(createProductDto);
        } catch (error) {
        throw new HttpException(error.message, HttpStatus.UNPROCESSABLE_ENTITY);
        }
    }

    @ApiParam({ name: 'id'})
    @Put(':id')
    async updateProduct(@Param('id') id,@Body() updateProductDto: UpdateProductDto): Promise<Product> {
        return await this.productService.updateProductAsync(id, updateProductDto);
    }

    // @ApiParam({ name: 'id'})
    // @Patch('active/:id')
    // async activeProduct(@Param('id') id): Promise<Product> {
    //     return await this.productService.activeProduct(id);
    // }
    @ApiParam({ name: 'id'})
    @Put(':id/status')
    async updateStatusAsync(
      @Param('id') id: string,
      @Body() _body: ChangeStatusProduct,
      @Res() res: Response,
    ): Promise<any> {
      return await this.productService.updateStatusAsync(
        id,
        _body.status,
        (item: any) => {
          if (!item) {
            return res.json({ 
                success: false,
                message: 'Status not valid' 
            });
          }
          return res.json(item);
        },
      );
      
    }

    @ApiParam({ name: 'id'})
    @Delete(':id')
    async deleteProduct(@Param('id') id)
    {
        return await this.productService.deleteProductAsync(id);
    }
}