export interface Product {
    id?: string;
    title: string;
    status: string;
    description: string;
    price: number;
}