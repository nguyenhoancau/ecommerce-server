import { Product } from "../interface/product.interface";
import { InjectModel } from "@nestjs/mongoose";
import { Model } from 'mongoose';
import { ProductDto } from "../dto/product.dto";

export class ProductRepo {
    constructor(
        @InjectModel('Product') private readonly productModel: Model<ProductDto>,
    ) {}
    async getAllAsync(): Promise<ProductDto[]> {
        return await this.productModel.find().exec();
    }

    async getProductByIdAsync(id: string): Promise<ProductDto> {
        return await this.productModel.findById(id).exec();
    }

    async createProductAsync(product: Product): Promise<ProductDto> {
        const newProduct = await new this.productModel(product);
        return newProduct.save();
    }

    async updateProductAsync(id: string, updateProduct): Promise<ProductDto> {
        return await this.productModel.findByIdAndUpdate(id, updateProduct, {
            new: true,
        });
    }

    async updateStateAsync(id: string, _status: string): Promise<ProductDto> {
        return this.productModel
          .findByIdAndUpdate(id, { status: _status })
          .then(res => this.getProductByIdAsync(id));
      }

    async deleteProductAsync(id: string): Promise<any> {
        return await this.productModel.findByIdAndRemove(id);
    }
} 