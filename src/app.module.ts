import { CarModule } from './car/car.module';
import { CarController } from './car/car.controller';
import dotenv from 'dotenv/config';
require('dotenv').config();
import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { ProductsModule } from './products/products.module';

@Module({
  imports: [
    CarModule,
    MongooseModule.forRoot(process.env.MONGO_URL, { useNewUrlParser: true }),
    ProductsModule,
  ],
  controllers: [CarController, AppController],
  providers: [AppService],
})
export class AppModule {}
